package drawing_coordinates;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javafx.scene.paint.Color;

public class DrawingCoordinates implements Serializable{


	private static final long serialVersionUID = 1362554568886281424L;
	private double x;
	private double y;

	public static final int STATE_PRESSED = 0;
	public static final int STATE_DRAGGED = 1;

	private int state;
	private Color color;


	public DrawingCoordinates (double x, double y) {

		this.x = x;
		this.y = y;
		color = new Color(0,0,0,1);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int getState () {
		return state;
	}

	public void setState (int state) {
		this.state = state;
	}
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}


	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	private void writeObject(ObjectOutputStream out) throws IOException {
		out.writeDouble(x);
		out.writeDouble(y);
		out.writeInt(state);
		out.writeUTF(color.toString());
	}
	
	private void readObject (ObjectInputStream in) throws ClassNotFoundException, IOException {
		
		this.x = in.readDouble();
		this.y = in.readDouble();
		this.state = in.readInt();
		this.color = Color.valueOf(in.readUTF());
	}
}
